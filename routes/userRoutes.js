const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');

const router = Router();

//GET all users ////////////////////////////////////////////////////////////////
router.get('/', (req, res, next) => {
    try {
        const listOfUsers = UserService.showList('names');
        if(!listOfUsers) {
            res.status(200).json({ messsage: "List of users is empty for now" });
        } else {
            let tempArray = [];
            listOfUsers.forEach(elem => {
                let { id, password, ...temp } = elem;
                tempArray.push(temp);
            });

            return res.status(200).json({ "All users": tempArray });
        }
    } catch(err) {
        next(err);
    }
});

//GET user with given ID //////////////////////////////////////////////////////////
router.get('/:id', (req, res, next) => {
    try {
        const findOfUser = UserService.search({ id: req.params.id });
        if(!findOfUser) {
            return res.status(404).json({ error: 404, message: "User not found" });
        } else {
            let { id, password, ...temp } = findOfUser;
            return res.status(200).json({ "User found": temp });
        }
    } catch(err) {
        next(err);
    }
});

//POST new user ////////////////////////////////////////////////////////////
router.post('/', createUserValid, (req, res, next) => {
    try {
        const createOfUser = UserService.createUser(req.body);
        if(!createOfUser) {
            return res.status(400).json({ error: 400, message: "Failed to create user" });
        } else {
            let { id, password, ...temp } = createOfUser;
            return res.status(200).json({ "User created": temp });
        }
    } catch(err) {
        next(err);
    }
});

//PUT update current user ////////////////////////////////////////////////////////////
router.put('/:id', (req, res, next) => {
    try {
        if(!UserService.search({ id: req.params.id })) {
            return res.status(404).json({ error: 404, message: "User not found" });
        } else {
            next();
        }
    } catch(err) {
        next(err);
    }
}, createUserValid, (req, res, next) => {
    try {
        const updateOfUser = UserService.updateUser(req.params.id, req.body);
        if(!updateOfUser) {
            return res.status(400).json({ error: 400, message: "Failed to update user" });
        } else {
            let { id, password, ...temp } = updateOfUser;
            return res.status(200).json({ "User updated": temp });
        }
    } catch(err) {
        next(err);
    }
});

//DELETE current user ///////////////////////////////////////////////////////////////////
router.delete('/:id', (req, res, next) => {
    try {
        const deleteOfUser = UserService.deleteUser(req.params.id);
        if(!deleteOfUser) {
            return res.status(404).json({ error: 404, message: "User not found" });
        } else {
            let { id, password, ...temp } = deleteOfUser;
            return res.status(200).json({ "User deleted": temp });
        }
    } catch(err) {
        next(err);
    }
});

router.use((err, req, res, next) => {
    return res.status(500).json({ error: 500, message: "Server error. Please try again later" });
});

module.exports = router;