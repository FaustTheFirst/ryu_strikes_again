const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

//GET all fighters ////////////////////////////////////////////////////////////////
router.get('/', (req, res, next) => {
    try {
        const listOfFighters = FighterService.showList('names');
        if(!listOfFighters) {
            res.status(200).json({ messsage: "List of fighters is empty for now" });
        } else {
            let tempArray = [];
            listOfFighters.forEach(elem => {
                let { id, password, ...temp } = elem;
                tempArray.push(temp);
            });

            return res.status(200).json({ "All fighters": tempArray });
        }
    } catch(err) {
        next(err);
    }
});

//GET fighter with given ID //////////////////////////////////////////////////////////
router.get('/:id', (req, res, next) => {
    try {
        const findOfFighter = FighterService.search({ id: req.params.id });
        if(!findOfFighter) {
            return res.status(404).json({ error: 404, message: "Fighter not found" });
        } else {
            let { id, password, ...temp } = findOfFighter;
            return res.status(200).json({ "Fighter found": temp });
        }
    } catch(err) {
        next(err);
    }
});

//POST new fighter ////////////////////////////////////////////////////////////
router.post('/', createFighterValid, (req, res, next) => {
    try {
        const createOfFighter = FighterService.createFighter(req.body);
        if(!createOfFighter) {
            return res.status(400).json({ error: 400, message: "Failed to create fighter" });
        } else {
            let { id, password, ...temp } = createOfFighter;
            return res.status(200).json({ "Fighter created": temp });
        }
    } catch(err) {
        next(err);
    }
});

//PUT update current fighter ////////////////////////////////////////////////////////////
router.put('/:id', (req, res, next) => {
    try {
        if(!FighterService.search({ id: req.params.id })) {
            return res.status(404).json({ error: 404, message: "Fighter not found" });
        } else {
            next();
        }
    } catch(err) {
        next(err);
    }
}, createFighterValid, (req, res, next) => {
    try {
        const updateOfFighter = FighterService.updateFighter(req.params.id, req.body);
        if(!updateOfFighter) {
            return res.status(400).json({ error: 400, message: "Failed to update fighter" });
        } else {
            let { id, password, ...temp } = updateOfFighter;
            return res.status(200).json({ "Fighter updated": temp });
        }
    } catch(err) {
        next(err);
    }
});

//DELETE current fighter ///////////////////////////////////////////////////////////////////
router.delete('/:id', (req, res, next) => {
    try {
        const deleteOfFighter = FighterService.deleteFighter(req.params.id);
        if(!deleteOfFighter) {
            return res.status(404).json({ error: 404, message: "Fighter not found" });
        } else {
            let { id, password, ...temp } = deleteOfFighter;
            return res.status(200).json({ "Fighter deleted": temp });
        }
    } catch(err) {
        next(err);
    }
});

router.use((err, req, res, next) => {
    return res.status(500).json({ error: 500, message: "Server error. Please try again later" });
});

module.exports = router;