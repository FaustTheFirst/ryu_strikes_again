const { fighter } = require('../models/fighter');

const createFighterValid = (req, res, next) => {

    try {
        const temp = {}; // For storing error messsages during validation ////////////////////////////////
        
        // Check if POST request has all remaining fields /////////////////////////////////////////////

        if(req.method === 'POST') {
            Object.keys(fighter).filter(elem => elem !== 'id').forEach(elem => {
                if(!req.body.hasOwnProperty(elem)) {
                    temp[elem] = `${elem} is required`;
                }
            });
        }
        
        // Check if body of request has empty fields, too long fields, ///////////////////////////////
        // or if field is wrong for creating or changing of fighter /////////////////////////////////////

        Object.keys(req.body).forEach(elem => {
            if(elem === 'id' ||
            elem === 'createdAt' ||
            elem === 'updatedAt') {
                temp[elem] = 'This property is not available';
                return void 0;
            } else if(!fighter.hasOwnProperty(elem)) {
                temp[elem] = `${elem} is not defined for fighter`;
                return void 0;
            }

            if(req.body[elem] === '') {
                temp[elem] = `${elem} is empty`;
            } else if(req.body[elem].length > 64) {
                temp[elem] = `${elem} exceeds the characters limit`;
            } else if(elem !== 'name' && typeof elem !== 'number') {
                temp[elem] = `${elem} must be a number`;
            }
        });

        if(req.body.health &&
            !temp.health &&
            (req.body.health > 100 ||
            req.body.health < 20)) {
                temp.health = "Health value must be from 20 to 100";
        }

        if(req.body.power &&
            !temp.power &&
            (req.body.power > 10 ||
            req.body.power < 1)) {
                temp.power = "Power value must be from 1 to 10";
        }

        if(req.body.defense &&
            !temp.defense &&
            (req.body.defense > 10 ||
            req.body.defense < 1)) {
                temp.defense = "Power value must be from 1 to 10";
        }

        // Show all validation errors if they exist /////////////////////////////////////////////////////////

        if(!(Object.entries(temp).length === 0)) {
            return res.status(400).json({ error: 400, message: temp });
        } else {
            next();
        }
    } catch(err) {
        next(err);
    }
}

const updateFighterValid = (req, res, next) => {
    // All validation happens in createUserValid ////////////////////////////////////////
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;