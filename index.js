const express = require('express')
const cors = require('cors');
require('./config/db');

const app = express()

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const routes = require('./routes/index');
routes(app);

app.get('/', (req, res) => {
    res.send('This is the homepage');
});

app.get('*', (req, res) => {
    res.status(404).send('This page is not found');
})

const port = 3050;
app.listen(port, () => {});

exports.app = app;